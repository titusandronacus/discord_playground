#!/usr/bin/env python
"""
Bot component
"""
import os
import logging

import discord
from discord.ext import commands
from discord.ext.commands import Context

from .gpt3_prompt import PromptHandler

logger = logging.getLogger('discord')

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='$', intents=intents)


@bot.event
async def on_ready():
    logger.debug(f'Logged in as {bot.user} ID: {bot.user.id}')


@bot.command()
async def prompt(ctx: Context, *, prompt_response: PromptHandler):
    """
    send gpt3 a text prompt to complete
    """
    logger.debug(f'Got message: {ctx.message}')
    await ctx.send(prompt_response)
