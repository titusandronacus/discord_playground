"""
GPT3 prompt component
"""
import logging
import os

import openai
from discord.ext.commands import Converter, Context

logger = logging.getLogger(name='discord')


class PromptHandler(Converter):
    async def convert(self, ctx: Context, prompt: str):
        completion = openai.Completion.create(engine=os.getenv('OPENAI_ENGINE'), prompt=prompt, max_tokens=1024)
        logger.debug(completion.choices[0].text)
        return completion.choices[0].text
