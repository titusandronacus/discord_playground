"""

"""
import os
import logging

from dotenv import load_dotenv

from .discord_bot import bot as discord_bot
import openai

# we'll strap all the components together at this module and run the module from here

handler = logging.StreamHandler()

load_dotenv()

openai.api_key = os.getenv('OPENAI_API_KEY')


def main():
    discord_bot.run(token=os.getenv('DISCORD_BOT_TOKEN'), log_handler=handler, log_level=logging.DEBUG)


if __name__ == "__main__":
    main()
