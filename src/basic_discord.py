#!/usr/bin/env python
import os
import logging

from dotenv import load_dotenv
import discord

handler = logging.StreamHandler()

intents = discord.Intents.default()
intents.message_content = True

load_dotenv()

client = discord.Client(intents=intents)

@client.event
async def on_ready():
    print(f'we have logged in as {client.user}')

@client.event
async def on_message(message: discord.Message):
    if message.author == client.user:
        return

    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')

client.run(token=os.getenv('DISCORD_BOT_TOKEN'), log_handler=handler, log_level=logging.DEBUG)
