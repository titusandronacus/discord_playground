# Discord Playground

## Description

Small project to work with discord.py and openai.py. A basic bot that will tell you hello if you give it a hello prompt,
and a prompt bot that will take prompts for GPT3 and give you the resulted response

## Install

First install poetry for you system if you haven't already: https://python-poetry.org/docs/

Clone the project, then run from the project root

```
poetry install
```

This will install all dependencies needed to run the bots.

You'll need an .env file set up in your project for some configuration. Below us the environment variables you need and
a sample of what you can put into the .env file:

```
DISCORD_BOT_TOKEN='discord bot token'
OPENAI_API_KEY='openai token'
OPENAI_ENGINE='text-ada-001'
```

### Reference for environment variables

#### DISCORD_BOT_TOKEN

You'll need to create a discord bot and get a token for it. Follow the instructions at https://discordpy.readthedocs.io/en/stable/discord.html
#### OPENAI_API_KEY

You'll need to create an account and generate an API key to use openai programmatically. See https://beta.openai.com/docs/quickstart/build-your-application for a primer and if you're logged in you can generate a key from here

#### OPENAI_ENGINE

A list of available engines can be obtained from https://beta.openai.com/docs/models/overview. The one listed in the sample
 is the fastest GPT-3 model

## Usage
To run the basic bot:
```commandline
poetry run run_basic_bot
```
To run the GPT3 prompt bot:
```commandline
poetry run run_prompt_bot
```

## Project purpose and status
This project is for me to get my feet wet with some discord bot work and a little work with GPT3. I'll probably use this as a party trick on discord every now and then, and will also continue to experiment on here, but probably won't be entertaining contributions or support requests. Feel free to fork as you see fit.

## License
This project is licensed under a MIT license